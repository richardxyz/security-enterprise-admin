import RenQuyuTree from './src/ren-quyu-tree'
RenQuyuTree.install = function (Vue) {
  Vue.component(RenQuyuTree.name, RenQuyuTree)
}
export default RenQuyuTree
