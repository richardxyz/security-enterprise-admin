import EmployeeDeptTree from './src/employee-dept-tree'
EmployeeDeptTree.install = function (Vue) {
  Vue.component(EmployeeDeptTree.name, EmployeeDeptTree)
}
export default EmployeeDeptTree
