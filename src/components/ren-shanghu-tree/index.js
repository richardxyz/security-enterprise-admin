import RenShanghuTree from './src/ren-shanghu-tree'
RenShanghuTree.install = function (Vue) {
  Vue.component(RenShanghuTree.name, RenShanghuTree)
}
export default RenShanghuTree
